﻿#Installation du DHCP (fonctionnalitée)
Install-WindowsFeature -Name DHCP -IncludeManagementTools

#Ajoute les users admins etc dans des groupe se sécurité
Add-DhcpServerSecurityGroup
#redemarrer le service dhcp
Restart-Service dhcpserver

#liaison avec la carte réseau qui porte le nom « Ethernet3 pour ma vm » pour IPv4.
Set-DhcpServerv4Binding -BindingState $True -InterfaceAlias "Ethernet3" 
#Creation de la premiere etendue DHCP ipv4
Add-DhcpServerv4Scope -Name "Etendue1" -StartRange 172.17.138.0 -EndRange 172.17.159.255 -SubnetMask 255.255.224.0 

#commencera à 172.17.138.1
#réservé apres 172.17.159.254

#DNS peut s'en servir si jamais on a plusieurs DC dans une entreprise

#ajout de l'option d'étendue pour la passerelle

#Set-DhcpServerv4OptionValue -Etendue1 172.17.138.181
#ajout de l'option d'étendue pour le serveur DNS

#Set-DhcpServerv4OptionValue -Etendue1 172.17.138.181

#Attention si on créer une foret, l'ip qui aura été mis de base sur la machine sera pris en compte ( à modifié avant de manipuler tout :) ) 

#WAASSSUPPP WASSSAUPP   BITCONNEEEEEECT